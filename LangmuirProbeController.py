#!/usr/bin/env python
# -*- coding:utf-8 -*-


# ############################################################################
#  license :
# ============================================================================
#
#  File :        LangmuirProbeController.py
#
#  Project :     LangmuirProbe
#
# This file is part of Tango device class.
# 
# Tango is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Tango is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Tango.  If not, see <http://www.gnu.org/licenses/>.
# 
#
#  $Author :      mark.amato$
#
#  $Revision :    $
#
#  $Date :        $
#
#  $HeadUrl :     $
# ============================================================================
#            This file is generated by POGO
#     (Program Obviously used to Generate tango Object)
# ############################################################################

__all__ = ["LangmuirProbeController", "LangmuirProbeControllerClass", "main"]

__docformat__ = 'restructuredtext'

import PyTango
import sys
# Add additional import
#----- PROTECTED REGION ID(LangmuirProbeController.additionnal_import) ENABLED START -----#
import numpy as np
#----- PROTECTED REGION END -----#	//	LangmuirProbeController.additionnal_import

# Device States Description
# ON : 
# MOVING : 
# RUNNING : 
# FAULT : 
# INIT : 
# OFF : 


class LangmuirProbeController (PyTango.LatestDeviceImpl):
    """This class controls the Langmuir Controller electronics.
    
    Functions
    
    - Init
    >> homes the unit and voltage sweeps.
    
    - Sweep
    >> sweeps the probe and takes current measurements as a function of voltage as a function of position
    
    
    Attributes
    -> minimum sweep position, counts
    -> maximum sweep position, counts
    -> position sweep step, counts
    -> minimum voltage
    -> maximum voltage
    -> voltage step
    -> voltage sample factor
    
    image attribute
    -> x axis = voltage
    -> y axis = position, counts
    -> z = current"""
    
    # -------- Add you global variables here --------------------------
    #----- PROTECTED REGION ID(LangmuirProbeController.global_variables) ENABLED START -----#
    voltageIndex = 0
    positionIndex = 0
    
    #----- PROTECTED REGION END -----#	//	LangmuirProbeController.global_variables

    def __init__(self, cl, name):
        PyTango.LatestDeviceImpl.__init__(self,cl,name)
        self.debug_stream("In __init__()")
        LangmuirProbeController.init_device(self)
        #----- PROTECTED REGION ID(LangmuirProbeController.__init__) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.__init__
        
    def delete_device(self):
        self.debug_stream("In delete_device()")
        #----- PROTECTED REGION ID(LangmuirProbeController.delete_device) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.delete_device

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        self.attr_maximumSweepPosition_read = 0.0
        self.attr_maximumVoltage_read = 0.0
        self.attr_minimumSweepPosition_read = 0.0
        self.attr_minimumVoltage_read = 0.0
        self.attr_sweepStepSize_read = 0.0
        self.attr_voltageSampleFactor_read = 0.0
        self.attr_voltageStepSize_read = 0.0
        self.attr_currentScan_read = [[0.0]]
        #----- PROTECTED REGION ID(LangmuirProbeController.init_device) ENABLED START -----#
        self.set_state(PyTango.DevState.ON)
        
        # initial settings
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        self.attr_maximumSweepPosition_read = 0.0
        self.attr_maximumVoltage_read = 200.0
        self.attr_minimumSweepPosition_read = -100000.0
        self.attr_minimumVoltage_read = 0.0
        self.attr_sweepStepSize_read = -1000.0
        self.attr_voltageSampleFactor_read = 3.0
        self.attr_voltageStepSize_read = 1.0
        self.attr_currentScan_read = np.ndarray(shape=(1024,1024), dtype = float)
        
        
        self.ioThread = threading.Thread(target = self.IOMethod)
        self.ioThread.setDaemon(True)
        self.ioThread.start() 
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.init_device

    def always_executed_hook(self):
        self.debug_stream("In always_excuted_hook()")
        #----- PROTECTED REGION ID(LangmuirProbeController.always_executed_hook) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.always_executed_hook

    # -------------------------------------------------------------------------
    #    LangmuirProbeController read/write attribute methods
    # -------------------------------------------------------------------------
    
    def read_maximumSweepPosition(self, attr):
        self.debug_stream("In read_maximumSweepPosition()")
        #----- PROTECTED REGION ID(LangmuirProbeController.maximumSweepPosition_read) ENABLED START -----#
        attr.set_value(self.attr_maximumSweepPosition_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.maximumSweepPosition_read
        
    def write_maximumSweepPosition(self, attr):
        self.debug_stream("In write_maximumSweepPosition()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(LangmuirProbeController.maximumSweepPosition_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.maximumSweepPosition_write
        
    def read_maximumVoltage(self, attr):
        self.debug_stream("In read_maximumVoltage()")
        #----- PROTECTED REGION ID(LangmuirProbeController.maximumVoltage_read) ENABLED START -----#
        attr.set_value(self.attr_maximumVoltage_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.maximumVoltage_read
        
    def write_maximumVoltage(self, attr):
        self.debug_stream("In write_maximumVoltage()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(LangmuirProbeController.maximumVoltage_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.maximumVoltage_write
        
    def read_minimumSweepPosition(self, attr):
        self.debug_stream("In read_minimumSweepPosition()")
        #----- PROTECTED REGION ID(LangmuirProbeController.minimumSweepPosition_read) ENABLED START -----#
        attr.set_value(self.attr_minimumSweepPosition_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.minimumSweepPosition_read
        
    def write_minimumSweepPosition(self, attr):
        self.debug_stream("In write_minimumSweepPosition()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(LangmuirProbeController.minimumSweepPosition_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.minimumSweepPosition_write
        
    def read_minimumVoltage(self, attr):
        self.debug_stream("In read_minimumVoltage()")
        #----- PROTECTED REGION ID(LangmuirProbeController.minimumVoltage_read) ENABLED START -----#
        attr.set_value(self.attr_minimumVoltage_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.minimumVoltage_read
        
    def write_minimumVoltage(self, attr):
        self.debug_stream("In write_minimumVoltage()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(LangmuirProbeController.minimumVoltage_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.minimumVoltage_write
        
    def read_sweepStepSize(self, attr):
        self.debug_stream("In read_sweepStepSize()")
        #----- PROTECTED REGION ID(LangmuirProbeController.sweepStepSize_read) ENABLED START -----#
        attr.set_value(self.attr_sweepStepSize_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.sweepStepSize_read
        
    def write_sweepStepSize(self, attr):
        self.debug_stream("In write_sweepStepSize()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(LangmuirProbeController.sweepStepSize_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.sweepStepSize_write
        
    def read_voltageSampleFactor(self, attr):
        self.debug_stream("In read_voltageSampleFactor()")
        #----- PROTECTED REGION ID(LangmuirProbeController.voltageSampleFactor_read) ENABLED START -----#
        attr.set_value(self.attr_voltageSampleFactor_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.voltageSampleFactor_read
        
    def write_voltageSampleFactor(self, attr):
        self.debug_stream("In write_voltageSampleFactor()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(LangmuirProbeController.voltageSampleFactor_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.voltageSampleFactor_write
        
    def read_voltageStepSize(self, attr):
        self.debug_stream("In read_voltageStepSize()")
        #----- PROTECTED REGION ID(LangmuirProbeController.voltageStepSize_read) ENABLED START -----#
        attr.set_value(self.attr_voltageStepSize_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.voltageStepSize_read
        
    def write_voltageStepSize(self, attr):
        self.debug_stream("In write_voltageStepSize()")
        data = attr.get_write_value()
        #----- PROTECTED REGION ID(LangmuirProbeController.voltageStepSize_write) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.voltageStepSize_write
        
    def read_currentScan(self, attr):
        self.debug_stream("In read_currentScan()")
        #----- PROTECTED REGION ID(LangmuirProbeController.currentScan_read) ENABLED START -----#
        attr.set_value(self.attr_currentScan_read)
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.currentScan_read
        
    
    
            
    def read_attr_hardware(self, data):
        self.debug_stream("In read_attr_hardware()")
        #----- PROTECTED REGION ID(LangmuirProbeController.read_attr_hardware) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.read_attr_hardware


    # -------------------------------------------------------------------------
    #    LangmuirProbeController command methods
    # -------------------------------------------------------------------------
    
    def Cancel(self):
        """ Cancels the existing scan.
        """
        self.debug_stream("In Cancel()")
        #----- PROTECTED REGION ID(LangmuirProbeController.Cancel) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.Cancel
        
    def Pause(self):
        """ Pauses the existing scan.
        """
        self.debug_stream("In Pause()")
        #----- PROTECTED REGION ID(LangmuirProbeController.Pause) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.Pause
        
    def Resume(self):
        """ Resumes the paused scan.
        """
        self.debug_stream("In Resume()")
        #----- PROTECTED REGION ID(LangmuirProbeController.Resume) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.Resume
        
    def Initialize(self):
        """ Initializes the system. Homes the motors.
        """
        self.debug_stream("In Initialize()")
        #----- PROTECTED REGION ID(LangmuirProbeController.Initialize) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.Initialize
        
    def Sweep(self):
        """ Performs a langmuir sweep.
        """
        self.debug_stream("In Sweep()")
        #----- PROTECTED REGION ID(LangmuirProbeController.Sweep) ENABLED START -----#
        # trigger the scan.
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.Sweep
        

    #----- PROTECTED REGION ID(LangmuirProbeController.programmer_methods) ENABLED START -----#
    def SampleCurrent(self):
        
        current = 0
        for currentSampleIndex in range(self.attr_voltageSampleFactor_read):

            current = current + PyTango.DeviceProxy(self.langmuirSupplyConfig).read_attribute("currentReadback").value
            
        return (current/self.attr_voltageSampleFactor_read)
            
        
    
    def IOMethod(self):
        
        try:
            while True:
                
                if self.Cancel:
                    
                    #scan cancelled. Move to rest position.
                    self.Sweeping = False
                    self.Initialize = True
                    
                # Sweeping routine
                if self.Sweeping:
                    
                    # three axis sweep: go to position, then go to voltages and measure at each point, then go to next position.
                    
                    for 
                    
                    
                    
                    pass
                    
                if self.Initialize:
                    
                    self.LangmuirMotor  = PyTango.DeviceProxy(self.langmuirMotorConfig)
                    self.LangmuirMotor.Home()
                    self.LangmuirSupply = PyTango.DeviceProxy(self.langmuirSupplyConfig)
                    self.LangmuirSupply.write_attribute(self.enable,False)
                    
                    self.Initialize = False
                    
                
        except Exception as e:
            pass
        
    #----- PROTECTED REGION END -----#	//	LangmuirProbeController.programmer_methods

class LangmuirProbeControllerClass(PyTango.DeviceClass):
    # -------- Add you global class variables here --------------------------
    #----- PROTECTED REGION ID(LangmuirProbeController.global_class_variables) ENABLED START -----#
    
    #----- PROTECTED REGION END -----#	//	LangmuirProbeController.global_class_variables


    #    Class Properties
    class_property_list = {
        }


    #    Device Properties
    device_property_list = {
        'langmuirSupplyConfig':
            [PyTango.DevString, 
            "the tango device server for the langmuir power supply",
            [] ],
        'langmuirMotorConfig':
            [PyTango.DevString, 
            "the tango device server for the langmuir Motor",
            [] ],
        }


    #    Command definitions
    cmd_list = {
        'Cancel':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Pause':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Resume':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Initialize':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        'Sweep':
            [[PyTango.DevVoid, "none"],
            [PyTango.DevVoid, "none"]],
        }


    #    Attribute definitions
    attr_list = {
        'maximumSweepPosition':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'maximumVoltage':
            [[PyTango.DevFloat,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'unit': "V",
            } ],
        'minimumSweepPosition':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'minimumVoltage':
            [[PyTango.DevFloat,
            PyTango.SCALAR,
            PyTango.READ_WRITE],
            {
                'unit': "V",
            } ],
        'sweepStepSize':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'voltageSampleFactor':
            [[PyTango.DevDouble,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'voltageStepSize':
            [[PyTango.DevFloat,
            PyTango.SCALAR,
            PyTango.READ_WRITE]],
        'currentScan':
            [[PyTango.DevFloat,
            PyTango.IMAGE,
            PyTango.READ, 1024, 1024]],
        }


def main():
    try:
        py = PyTango.Util(sys.argv)
        py.add_class(LangmuirProbeControllerClass, LangmuirProbeController, 'LangmuirProbeController')
        #----- PROTECTED REGION ID(LangmuirProbeController.add_classes) ENABLED START -----#
        
        #----- PROTECTED REGION END -----#	//	LangmuirProbeController.add_classes

        U = PyTango.Util.instance()
        U.server_init()
        U.server_run()

    except PyTango.DevFailed as e:
        print ('-------> Received a DevFailed exception:', e)
    except Exception as e:
        print ('-------> An unforeseen exception occured....', e)

if __name__ == '__main__':
    main()
